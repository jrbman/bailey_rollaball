﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    public float speed;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    private int count;

    //creates the variable that will store our position at the start
    Vector3 originalPosition;

    //creates a game object that allows for inserting the particle prefab in the player controller
    public GameObject pickupPfxPrefab;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";

        //saves position at start of the game
        originalPosition = transform.position;
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        //creates the burst when a pickup is grabbed at the same place and rotation
        Instantiate(pickupPfxPrefab, other.transform.position, pickupPfxPrefab.transform.rotation);

        if (other.gameObject.CompareTag("pickup"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();

            //respawns ball to original position at the start when touching the pickup
            transform.position = originalPosition;
            //makes sure ball isn't moving when respawned
            rb.velocity = Vector3.zero;
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        //says how many pickups are needed to win. I changed it to the number of pickups I have in the play-area
        if (count >= 8)
        {
            winText.text = "You Win!";
        }
    }
}